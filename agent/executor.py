#!/usr/bin/poython3.6
# conding:utf-8
from subprocess import PIPE, Popen


class Executor:
    def run(self, script, timeout):
        p = Popen(script, shell=True, stdout=PIPE)
        code = p.wait(timeout=timeout)
        txt = p.stdout.read().decode()
        return (code, txt)


if __name__ == "__main__":
    exec = Executor()
    print(exec.run("echo  'hello'", 3))
