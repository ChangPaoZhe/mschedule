#!/usr/bin/poython3.6
# conding:utf-8
from .cm import Conn_Manager
import threading


class app:
    def __init__(self, timeout):
        self.conn = Conn_Manager(timeout)
        self.event = threading.Event()

    def start(self):
        while not self.event.is_set():
            try:
                self.conn.start()
            except  Exception  as e:
                print('重连')
                self.conn.shutdown()
            self.event.wait(3)

    def shutdown(self):
        self.event.set()
        self.conn.shutdown()


if __name__ == "__main__":
    pass
