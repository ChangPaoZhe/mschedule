#!/usr/bin/poython3.6
# conding:utf-8
from agent import app

if __name__ == "__main__":
    agent = app(3)
    try:
        agent.start()
    except  KeyboardInterrupt:
        agent.shutdown()
