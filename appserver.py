#!/usr/bin/poython3.6
# conding:utf-8

from master import appserver

if __name__ == "__main__":
    appserver = appserver()
    try:
        appserver.start()
    except KeyboardInterrupt:
        appserver.shutdown()
