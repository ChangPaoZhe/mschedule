#!/bin/bash
echo   -e "\033[32m----------欢迎安装mschedule调度系统----------\033[0m"
echo   -e "\033[32m----------pip虚拟配置文件插件导入配置----------\033[0m"
spwd=`pwd`
Source(){
        sip=`cat   $spwd/$1/config.py     | grep  $2  | cut  -d  ":"  -f 2 | cut  -d  '/'  -f 3`
        sport=`cat   $spwd/$1/config.py     | grep  $2  | cut  -d  ":"  -f 3  | cut  -d  '"'  -f 1`
        echo  $sip,$sport
}
changefile(){
        echo  -e  "\033[32m--------修改$1 IP地址----------\033[0m"
        Source  $1 $2
    read   -p  "请输入 $1 的IP地址,默认是 $sip:"  ip
    echo  "ip 地址为: $ip"
    read    -p  "请输入$1 监听的PORT地址,默认是 $sport:"  port
    echo  "port 地址为:$port"
    if  [[  $port  &&  $ip ]]
    then
        sed  -i  "s/$sip:$sport/$ip:$port/g" $spwd/$1/config.py
        echo  -e  "\033[32m---------配置 $1 文件修改成功----------\033[0m"
    else
        echo  -e  "\033[32m---------保持默认配置----------\033[0m"
    fi
}
checkport(){
        Source $1  $2
        N=`netstat -tunlp   | grep  $sport  | wc  -l`
        if [ $N  -eq 1 ]
        then
                echo  -e  "\033[32m $1 已经启动\033[0m"
        else
                python  $spwd/$3  &
                sleep 5
                $N=`netstat -tunlp   | grep  $sport  | wc  -l`
                if [ $N  -eq 1 ]
                then
                        echo  -e "\033[32m $1 启动成功，监听端口为: $sport\033[0m"
                else
                        echo  -e   "\033[31m $1 启动失败!!!!!!!!!!,请检查原因\033[0m"
                fi
        fi
}

pipinstall() {
        pip  install  -r  $spwd/setup
}
change(){
        if  [ $?  -eq 0 ]
        then
                changefile  master  MASTER_URL
                changefile  agent   CONN_URL
        fi
}
startapp(){
        n=`ps -axu | grep  app.py  | wc  -l`
        if  [ $n -gt 1 ]
        then
                echo  -e  "\033[32m 客户端服务已经启动\033[0m"
        else
                python   $spwd/app.py    &
                sleep  3
                n=`ps -axu | grep  app.py  | wc  -l`
                echo  $n
                if  [ $n -gt 1 ]
                then
                        echo -e  "\033[32m 客户端服务启动成功\033[0m"
                else
                        echo  -e  "\033[31m 客户端服务启动失败\033[0m"
                fi
        fi
}
changewebserver(){
        echo  -e  "\033[32m--------修改$1 IP地址----------\033[0m"
        sip=`cat  $spwd/$1   | grep  web.run_app | cut -d ','  -f 2 | cut -d  '='  -f 2`
        sport=`cat  $spwd/$1    | grep  web.run_app | cut -d ','  -f 3  | cut -d '='  -f 2  | cut -d ')'  -f 1`
        echo  "源IP地址为 :$sip  源端口为 $sport"
    read   -p  "请输入 $1 的IP地址,默认是 $sip:"  ip
    echo  "ip 地址为: $ip"
    read    -p  "请输入$1 监听的PORT地址,默认是 $sport:"  port
    echo  "port 地址为:$port"
    if  [[  $port  &&  $ip ]]
    then
        sed  -i  "s/$sip/'$ip'/g" $spwd/$1
        sed  -i  "s/$sport/$port/g" $spwd/$1
        echo  -e  "\033[32m---------配置 $1 文件修改成功----------\033[0m"
    else
        echo  -e  "\033[32m---------保持默认配置----------\033[0m"
    fi

}
startserverapp(){
        checkport master  MASTER_URL  appserver.py   &
}
startwebserver(){
        sport=`cat  $spwd/$1    | grep  web.run_app | cut -d ','  -f 3  | cut -d '='  -f 2  | cut -d ')'  -f 1`
        n=`netstat -tunlp   | grep  $sport  | wc  -l`
        if  [ $n -eq 1 ]
        then
                echo  -e  "\033[32m webserver 已经启动或者端口被占用\033[0m"
        else
                python  $spwd/$1 &
                sleep  5
                n=`netstat -tunlp   | grep  $sport  | wc  -l`
                if  [ $n -eq 1 ]
            then
            echo -e  "\033[32m webserver 启动成功\033[0m"
                else
                        echo -e  "\033[31m webserver 启动失败\033[0m"
                fi
        fi
}
PS3="请输入要执行的选项:"
select   name  in  安装虚拟环境插件 修改服务端和客户端监听IP和端口  修改webserver端监听IP和端口  启动服务端  启动客户端 启动webserver  退出
do
        case  $name  in
        "安装虚拟环境插件")
        pipinstall
        ;;
        "修改服务端和客户端监听IP和端口")
        change
        ;;
        "修改webserver端监听IP和端口")
        changewebserver  appwebserver.py
        ;;
        "启动服务端")
        startserverapp
        ;;
        "启动客户端")
        startapp
        ;;
        "启动webserver")
        startwebserver   appwebserver.py
        ;;
        "退出")
        exit 0
        ;;
        *)
        echo "输入错误，请重新输入"
        ;;
        esac
done
