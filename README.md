# mschedule

#### 介绍
此项目主要用于远程管理和调度，适用于Linux平台和Windows平台，其使用python编写，使用了C-S架构完成

#### 软件架构图如下

![输入图片说明](https://images.gitee.com/uploads/images/2019/1119/113537_6692d82b_1759676.png "屏幕截图.png")

#### 相关说明 
此架构主要分为四个部分
1. 第一部分是client端，一般为浏览器，主要用于创建任务和对任务执行结果的可视化
2. 第二部分是webappserver端，此处使用aiohttp和zerorpc实现，aiohttp 主要用于和client浏览器端进行http数据交互，而zerorpc主要用于和master调度程序之间交互，用于对client端创建的信息的上报和显示，其和master端的交互使用TCP链接完成
3. 第三部分是master调度程序，此处与webappserver中的 zerorpc客户端和agents客户端交互处理相关数据，此处使用zerorpc server来实现。
4. 第四部分是agent程序，主要用于获取master端数据并处理上报，自然也包括注册和心跳信息的处理操作        
    
#### 安装教程
方式一 适用于所有环境
1.  创建虚拟环境 
    相关创建详情网页获取
2.  导入相关插件
    pip install -r  requirements 
3.  启动服务
    1.  启动服务端 
    python   appserver.py
    2.  启动客户端
    python   app.py
    3.  启动webserver 
    python  appwebserver.py

方式二 适用于Linux环境
1. 创建虚拟环境相关创建详情网页获取
2. 配置相关配置和启动，可通过setup.sh进行处理和选择即可，其中必须注意，对应的客户端的IP,PORT和服务端的IP,PORT必须同时修改方才修改。
#### 使用说明

1.  欲修改客户端agent相关监听端口和IP地址，请修改agent/config.py 
2.  欲修改服务端master启动IP地址和端口，请修改master/config.py 
3.  欲修改web监听端口，请修改 appwebserver.py 



#### 相关博客如下:

https://blog.51cto.com/11233559/2451399
