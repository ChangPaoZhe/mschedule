#!/usr/bin/poython3.6
# conding:utf-8
import logging


def getlogger(mod_name: str, filepath: str = '/var/log/mschedule'):
    logger = logging.getLogger(mod_name)  # 获取名字
    logger.setLevel(logging.INFO)  # 添加日志级别
    logger.propagate = False  # 配置不想上传递
    handler = logging.FileHandler("{}/{}.log".format(filepath, mod_name))
    fmt = logging.Formatter("%(asctime)s [%(levelname)s] %(message)s (%(filename)s:L%(lineno)d)",
                            datefmt='%Y-%m-%d %H:%M:%S')
    handler.setFormatter(fmt)
    logger.addHandler(handler)
    return logger


if __name__ == "__main__":
    log = getlogger('test')
    log.info('13234545654')
