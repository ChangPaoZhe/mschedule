#!/usr/bin/poython3.6
# conding:utf-8
from .cm import Master_Listen


class appserver:
    def __init__(self):
        self.appserver = Master_Listen()

    def start(self):
        self.appserver.start()

    def shutdown(self):
        self.appserver.shutdown()


if __name__ == "__main__":
    pass
