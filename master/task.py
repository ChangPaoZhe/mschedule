#!/usr/bin/poython3.6
# conding:utf-8
import uuid  # 获取唯一的task_id

from .state import *


class Task:
    def __init__(self, task_id, script, targets, timeout=0, parallel=1, fail_rate=0, fail_count=-1):
        self.id = task_id  # task唯一标识，用于确定任务
        self.script = script  # 对应的脚本内容，客户端输入的脚本
        self.timeout = timeout  # 超时时间
        self.parallel = parallel  # 并行执行数量
        self.fail_rate = fail_rate  # 失败率
        self.fail_count = fail_count  # 失败数
        self.state = WAITING  # 对应的消息的状态
        self.targets = {agent_id: {'state': WAITING, 'output': ''} for agent_id in targets}  # 此处对应客户端列表
        self.target_count = len(self.targets)  # 此处对应客户端的数量


if __name__ == "__main__":
    pass
