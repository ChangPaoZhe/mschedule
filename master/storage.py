#!/usr/bin/poython3.6
# conding:utf-8

import datetime
from .task import Task
from .state import *
from utils import getlogger

log = getlogger('storage')


class Storage:
    def __init__(self):
        self.agents = {}  # 此处用于存储用户信息
        self.tasks = {}  # 此处用于存储作业信息
        self.result = {}  # 用于存储agent端返回的结果
        self.task_state = 0  # 用于处理当所有agent状态都修改为成功或失败时将task的状态也进行相关的修改

    def reg_hb(self, agent_id, info):  # id 及就是客户端的id ，info 及就是host和ip地址
        self.agents[agent_id] = {
            'heaerbeat': datetime.datetime.now().timestamp(),
            'info': info,
            'busy': self.agents.get(agent_id, {}).get('busy', False)
        }
        # busy 读不到置False,读到了不变

    def get_agents(self):
        return self.agents

    def add_task(self, task: dict):  # 此处用于从客户端获取相关的数据
        t = Task(**task)  # 此处进行参数解构
        self.tasks[t.id] = t
        return t.id  # 此处用于获取处理id

    @property
    def itme_task(self):
        yield from (task for task in self.tasks.values() if
                    task.state in {WAITING, RUNNING})  # 此处返回task,当其中有成功或者失败时，则不用进行相关的操作处理
        # 当为WAITING或者RUNNING 时，则进行相关的操作，其他情况则不进行相关操作

    def get_task(self, agent_id):
        for task in self.itme_task:
            if agent_id in task.targets:  # 此处用于判断当前节点接入任务情况
                if task.state == WAITING:
                    task.state = RUNNING  # 当前消息的状态
                task.targets[agent_id]['state'] = RUNNING  # 此处是指此消息中的agent是否执行的状态的处理,若获取了，则此处的状态为RUNNING
                return [task.id, task.script, task.timeout]

    def add_result(self, payload: dict):
        for task in self.itme_task:
            if payload['code'] == 0:
                task.targets[payload['agent_id']]['state'] = SUCCEED  # 此处是指对此消息进行处理，若code=0，则表示客户端执行成功，若为1，则表示失败
                self.task_state += 1
            else:
                task.targets[payload['agent_id']]['state'] = FAILED  #
                self.task_state += 1
            if self.task_state == task.target_count:
                task.state = SUCCEED
                self.task_state = 0
            payload['agent_state'] = task.targets[payload['agent_id']]['state']
        log.info("当前消息内容为：{}".format(self.result))
        self.result[payload['id']] = payload  # 此处以task_id 为键，以payload为值进行处理

    def get_result(self, task_id: dict):
        task_id = task_id['task_id']
        return self.result.get(task_id)  # task_id,获取对应的payload值


if __name__ == "__main__":
    pass
