#!/usr/bin/poython3.6
# conding:utf-8
from utils import getlogger
from .config import MASTER_URL
import zerorpc
from .handler import Handler

log = getlogger('server')


class Master_Listen:
    def __init__(self):
        self.server = zerorpc.Server(Handler())

    def start(self):
        self.server.bind(MASTER_URL)
        log.info('Master 启动配置')
        self.server.run()

    def shutdown(self):
        self.server.close()


if __name__ == "__main__":
    pass
